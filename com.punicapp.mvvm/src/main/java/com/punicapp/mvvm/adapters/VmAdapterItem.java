package com.punicapp.mvvm.adapters;

public class VmAdapterItem {
    private Object object;
    private int itemType;

    public VmAdapterItem(Object object, int itemType) {
        this.object = object;
        this.itemType = itemType;
    }

    public Object getObject() {
        return object;
    }

    public int getItemType() {
        return itemType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VmAdapterItem that = (VmAdapterItem) o;

        if (itemType != that.itemType) return false;
        return object != null ? object.equals(that.object) : that.object == null;
    }

    @Override
    public int hashCode() {
        int result = object != null ? object.hashCode() : 0;
        result = 31 * result + itemType;
        return result;
    }
}
