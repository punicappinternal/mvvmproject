package com.punicapp.mvvm.adapters;

import com.google.common.base.Supplier;
import com.punicapp.mvvm.android.AppViewModel;

public abstract class AdapterItemViewModel<T, U extends AppViewModel> {
    protected U parent;
    protected Supplier<Integer> adapterPosition;

    public void init(U parent) {
        this.parent = parent;
    }

    public void setAdapterPosition(Supplier<Integer> adapterPosition) {
        this.adapterPosition = adapterPosition;
    }

    public abstract void bindData(T data);
}
