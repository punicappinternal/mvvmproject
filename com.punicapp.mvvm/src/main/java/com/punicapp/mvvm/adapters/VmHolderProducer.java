package com.punicapp.mvvm.adapters;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.LayoutRes;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.punicapp.mvvm.android.AppViewModel;

public class VmHolderProducer<T, VM extends AdapterItemViewModel<T, AppVM>, AppVM extends AppViewModel> {

    private final int viewType;
    protected AppVM parent;
    private final Class<T> modelType;
    private final Class<VM> viewModelType;
    @LayoutRes
    private final int layoutResource;
    private int viewModelVarId;

    public VmHolderProducer(int viewType, @LayoutRes int layoutResource, Class<T> modelType, Class<VM> viewModelType) {
        this.viewType = viewType;
        this.modelType = modelType;
        this.viewModelType = viewModelType;
        this.layoutResource = layoutResource;
    }

    public void setParent(AppVM parent) {
        this.parent = parent;
    }

    public void setViewModelVarId(int viewModelVarId) {
        this.viewModelVarId = viewModelVarId;
    }

    public VmViewHolder<T, VM> produce(ViewGroup parent) {
        VM vModel = instantiateViewModel(this.parent);

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ViewDataBinding binding = DataBindingUtil.inflate(inflater, layoutResource, parent, false);
        initViewModel(vModel, binding);
        return new VmViewHolder<>(binding, vModel, viewModelVarId);
    }

    protected void initViewModel(VM vModel, ViewDataBinding binding) {
    }

    public int getViewType() {
        return viewType;
    }

    public Class<T> getModelType() {
        return modelType;
    }

    private VM instantiateViewModel(AppVM processor) {
        Class<VM> uClass = getViewModelType();
        try {
            VM vModel = uClass.newInstance();
            vModel.init(processor);
            return vModel;
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public Class<VM> getViewModelType() {
        return viewModelType;
    }

}
