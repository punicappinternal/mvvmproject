package com.punicapp.mvvm.adapters;

import java.util.List;

public class DefaultDataProcessor extends AdapterDataProcessor {
    @Override
    protected void processData(VmAdapter adapter, List<VmAdapterItem> oldData, List<VmAdapterItem> newData) {
        oldData.clear();
        oldData.addAll(newData);
        adapter.notifyDataSetChanged();
    }
}
