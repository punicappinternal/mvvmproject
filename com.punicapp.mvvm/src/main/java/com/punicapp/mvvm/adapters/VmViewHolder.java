package com.punicapp.mvvm.adapters;

import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;

import com.google.common.base.Supplier;
import com.punicapp.mvvm.android.AppViewModel;

public class VmViewHolder<T, U extends AdapterItemViewModel<T, ? extends AppViewModel>> extends RecyclerView.ViewHolder {
    private U vModel;
    private ViewDataBinding binding;

    VmViewHolder(ViewDataBinding binding, U vModel, int viewModelVarId) {
        super(binding.getRoot());
        this.binding = binding;
        this.vModel = vModel;
        vModel.setAdapterPosition(new Supplier<Integer>() {
            @Override
            public Integer get() {
                return getAdapterPosition();
            }
        });
        binding.setVariable(viewModelVarId, vModel);
    }

    public void bindData(T item) {
        vModel.bindData(item);
    }
}
