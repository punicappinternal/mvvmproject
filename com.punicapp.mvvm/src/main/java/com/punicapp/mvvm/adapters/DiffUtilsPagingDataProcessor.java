package com.punicapp.mvvm.adapters;

import android.support.v7.util.DiffUtil;

import com.google.common.base.Objects;

import java.util.ArrayList;
import java.util.List;

public class DiffUtilsPagingDataProcessor extends DiffUtilsDataProcessor {
    @Override
    protected void processData(VmAdapter adapter, List<VmAdapterItem> oldData, List<VmAdapterItem> newData) {
        List<VmAdapterItem> items = new ArrayList<>();
        items.addAll(oldData);
        items.addAll(newData);
        DiffUtil.DiffResult result = DiffUtil.calculateDiff(new VmAdapterItemCallback(oldData, items), false);
        oldData.addAll(newData);
        result.dispatchUpdatesTo(adapter);
    }

    protected static class VmAdapterItemCallback extends DiffUtil.Callback {
        private final List<VmAdapterItem> oldData;
        private final List<VmAdapterItem> newData;

        VmAdapterItemCallback(List<VmAdapterItem> oldData, List<VmAdapterItem> newData) {
            this.oldData = oldData;
            this.newData = newData;
        }

        @Override
        public int getOldListSize() {
            return oldData.size();
        }

        @Override
        public int getNewListSize() {
            return newData.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            Object newObject = newData.get(newItemPosition).getObject();
            Object oldObject = oldData.get(oldItemPosition).getObject();
            return newObject.equals(oldObject);
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            Object newObject = newData.get(newItemPosition).getObject();
            Object oldObject = oldData.get(oldItemPosition).getObject();
            return Objects.equal(newObject, oldObject);
        }
    }
}
