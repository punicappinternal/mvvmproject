package com.punicapp.mvvm.adapters;

import android.databinding.Bindable;

import java.util.List;

public abstract class AdapterDataProcessor {

    protected abstract void processData(VmAdapter adapter, List<VmAdapterItem> oldData, List<VmAdapterItem> newData);

    public void processInvalidation(VmAdapter vmAdapter, List<Integer> list) {
        for (Integer i : list) {
            vmAdapter.notifyItemChanged(i);
        }
    }

    public void processRemoving(VmAdapter vmAdapter, List<Integer> list) {
        int min = vmAdapter.getItemCount();
        for (Integer i : list) {
            requestRemove(vmAdapter, i);
            vmAdapter.notifyItemRemoved(i);
            if (i < min) {
                min = i;
            }
        }
    }

    private void requestRemove(VmAdapter vmAdapter, Integer i) {
        vmAdapter.removeItem(i);
    }
}
