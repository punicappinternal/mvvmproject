package com.punicapp.mvvm.adapters;

import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.punicapp.mvvm.android.AppViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.subjects.PublishSubject;

public class VmAdapter extends RecyclerView.Adapter<VmViewHolder> implements IRecyclerObserver {
    @Override
    public int getSize() {
        return getItemCount();
    }

    @Override
    public void clearAll() {
        reset();
    }

    public static class Builder<VM extends AppViewModel> {
        private SparseArray<VmHolderProducer> producers = new SparseArray<>();
        private Map<Class, Integer> index = new HashMap<>();
        private AdapterDataProcessor dataProcessor = new DiffUtilsDataProcessor();
        private VM appModel;
        private int viewModelVarId;

        public Builder(VM appModel, int viewModelVarId) {
            this.appModel = appModel;
            this.viewModelVarId = viewModelVarId;
        }

        public <T, U extends AdapterItemViewModel<T, VM>>
        Builder<VM> defaultProducer(int type, @LayoutRes int layoutId, Class<T> modelClass, Class<U> adapterViewModelClass) {
            VmHolderProducer<T, U, VM> producer = new VmHolderProducer<>(type, layoutId, modelClass, adapterViewModelClass);
            return producer(producer);
        }

        public <T, U extends AdapterItemViewModel<T, VM>> Builder<VM> producer(VmHolderProducer<T, U, VM> producer) {
            producer.setViewModelVarId(viewModelVarId);
            producer.setParent(appModel);
            index.put(producer.getModelType(), producer.getViewType());
            producers.put(producer.getViewType(), producer);
            return this;
        }

        public Builder<VM> processor(AdapterDataProcessor processor) {
            dataProcessor = processor;
            return this;
        }

        public VmAdapter build() {
            return new VmAdapter(producers, index, dataProcessor);
        }
    }

    private List<VmAdapterItem> data = new ArrayList<>();

    private final PublishSubject<List> dataReceiver = PublishSubject.create();
    private final PublishSubject<List<VmAdapterItem>> dataWithTypeReceiver = PublishSubject.create();
    private final PublishSubject<List<Integer>> dataInvalidator = PublishSubject.create();
    private final PublishSubject<List<Integer>> dataEraser = PublishSubject.create();

    private SparseArray<VmHolderProducer> producers;
    private Map<Class, Integer> index;

    private final CompositeDisposable dataSubscription = new CompositeDisposable();

    public Observer<List> getDataReceiver() {
        return dataReceiver;
    }

    public Observer<List<VmAdapterItem>> getDataWithTypeReceiver() {
        return dataWithTypeReceiver;
    }

    public Observer<List<Integer>> getDataInvalidator() {
        return dataInvalidator;
    }

    public Observer<List<Integer>> getDataEraser() {
        return dataEraser;
    }

    private VmAdapter(SparseArray<VmHolderProducer> producers, Map<Class, Integer> index,
                      final AdapterDataProcessor dataPrcessor) {
        this.producers = producers;
        this.index = index;

        Observable<List<VmAdapterItem>> dataTransformed = dataReceiver.map(new Function<List, List<VmAdapterItem>>() {
            @Override
            public List<VmAdapterItem> apply(List list) {
                ArrayList<VmAdapterItem> vmAdapterItemList = new ArrayList<>();
                for (Object o : list) {
                    vmAdapterItemList.add(new VmAdapterItem(o, VmAdapter.this.getTypeByItem(o)));
                }
                return vmAdapterItemList;
            }
        });

        Disposable processAddingWithType = Observable
                .merge(dataWithTypeReceiver, dataTransformed)
                .subscribe(new Consumer<List<VmAdapterItem>>() {
                    @Override
                    public void accept(List<VmAdapterItem> list) {
                        dataPrcessor.processData(VmAdapter.this, data, list);
                    }
                });
        dataSubscription.add(processAddingWithType);

        Disposable processInvalidation = dataInvalidator.subscribe(new Consumer<List<Integer>>() {
            @Override
            public void accept(List<Integer> list) {
                dataPrcessor.processInvalidation(VmAdapter.this, list);
            }
        });
        dataSubscription.add(processInvalidation);

        Disposable processRemoving = dataEraser.subscribe(new Consumer<List<Integer>>() {
            @Override
            public void accept(List<Integer> list) {
                dataPrcessor.processRemoving(VmAdapter.this, list);
            }
        });
        dataSubscription.add(processRemoving);
    }

    private int getTypeByItem(Object item) {
        Class<?> clz = item.getClass();
        Set<Class> classes = index.keySet();
        for (Class c : classes) {
            if (c.isAssignableFrom(clz)) {
                return index.get(c);
            }
        }
        throw new IllegalStateException("Holder producer not found!");
    }

    @NonNull
    @Override
    public VmViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        VmHolderProducer producer = producers.get(viewType);
        if (producer == null) {
            throw new IllegalStateException("Holder producer not found!");
        }
        return producer.produce(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull VmViewHolder holder, int position) {
        VmAdapterItem item = getItem(position);
        if (item != null) {
            holder.bindData(item.getObject());
        }
    }

    private VmAdapterItem getItem(int position) {
        if (position >= 0 && position < data.size()) {
            return data.get(position);
        } else {
            return null;
        }
    }

    void removeItem(Integer i) {
        int index = i;
        data.remove(index);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        VmAdapterItem adapterItem = getItem(position);
        if (adapterItem != null) {
            return adapterItem.getItemType();
        }
        throw new RuntimeException("adapterItem is null!");
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        dataSubscription.dispose();
    }

    public void reset() {
        data.clear();
    }
}
