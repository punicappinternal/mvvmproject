package com.punicapp.mvvm.adapters;

public interface IRecyclerObserver {
    int getSize();
    void clearAll();
}
