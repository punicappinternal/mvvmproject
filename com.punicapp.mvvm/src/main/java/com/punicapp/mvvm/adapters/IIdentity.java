package com.punicapp.mvvm.adapters;

public interface IIdentity {
    String getId();
}