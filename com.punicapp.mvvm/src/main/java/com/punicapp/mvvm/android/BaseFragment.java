package com.punicapp.mvvm.android;


import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.common.reflect.TypeToken;
import com.punicapp.mvvm.actions.UIActionConsumer;

import io.reactivex.functions.Consumer;

public abstract class BaseFragment<T extends AppViewModel> extends Fragment {

    protected View rootView;
    private UIActionConsumer consumer;
    private T appViewModel;
    private ExcConsumer errorHandler = new ExcConsumer();
    private final Class<T> rawType = (Class<T>) new TypeToken<T>(getClass()) {
    }.getRawType();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appViewModel = configureViewModel();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = initBinding(inflater, container, appViewModel);

        consumer = new UIActionConsumer();
        fillHandlers(consumer);
        appViewModel.listen(consumer, errorHandler);

        return rootView;
    }

    protected void handleSubjecriptionError(Throwable throwable) {

    }

    private T configureViewModel() {
        T appViewModel = initViewModel(rawType);
        getLifecycle().addObserver(appViewModel);
        return appViewModel;
    }

    protected void fillHandlers(UIActionConsumer consumer) {
    }

    protected abstract View initBinding(LayoutInflater inflater, ViewGroup container, T appViewModel);

    @Nullable
    protected String getKey() {
        return null;
    }

    private T initViewModel(Class<T> clazz) {
        FragmentActivity activity = getActivity();
        if (activity == null) {
            return null;
        }

        ViewModelProvider provider = ViewModelProviders.of(activity);
        String key = getKey();
        return key == null ? provider.get(clazz) : provider.get(key, clazz);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getLifecycle().removeObserver(appViewModel);
    }

    private class ExcConsumer implements Consumer<Throwable> {
        @Override
        public void accept(Throwable throwable) {
            handleSubjecriptionError(throwable);
        }
    }
}
