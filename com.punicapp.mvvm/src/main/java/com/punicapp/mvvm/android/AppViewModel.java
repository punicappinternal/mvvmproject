package com.punicapp.mvvm.android;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import com.punicapp.mvvm.actions.UIAction;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.subjects.PublishSubject;

public class AppViewModel extends AndroidViewModel implements LifecycleObserver {

    protected PublishSubject<UIAction> processor = PublishSubject.create();
    protected CompositeDisposable disposable;

    public AppViewModel(@NonNull Application application) {
        super(application);
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    protected void doOnResume() {
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    protected void doOnDestroy() {
        clearResources();
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    protected void doOnPause() {

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    protected void doOnStart() {

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    protected void doOnStop() {

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    protected void doOnCreate() {

    }

    private void clearResources() {
        if (disposable == null) {
            return;

        }
        disposable.dispose();
        disposable = null;
        return;
    }

    protected void listen(Consumer<UIAction> consumer, Consumer<? super Throwable> errorHandler) {
        if (disposable == null) {
            disposable = new CompositeDisposable();
        }
        Disposable subscribe = processor.subscribe(consumer, errorHandler);
        disposable.add(subscribe);
    }


}
