package com.punicapp.mvvm.android;

import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.common.reflect.TypeToken;
import com.punicapp.mvvm.R;

import java.lang.reflect.Method;

public abstract class BaseActivity<T extends ViewDataBinding> extends AppCompatActivity {
    protected View container;
    protected Fragment fragment;
    protected T binding;
    private final Class<? super T> bindingClass = new TypeToken<T>(getClass()) {
    }.getRawType();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = inflateBinding();
        setContentView(binding.getRoot());
        onCreateContent();
        initFragment();
    }

    private T inflateBinding() {
        try {
            Method method = bindingClass.getMethod("inflate", LayoutInflater.class, ViewGroup.class, boolean.class);
            T binding = (T) method.invoke(null, LayoutInflater.from(this), null, false);
            return binding;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    protected void onCreateContent() {
        container = findContainerView();
        checkContainer(container);
    }

    private void checkContainer(View container) {
        if (container == null) {
            throw new NullPointerException("Container must not be null. Do you have view with id base_activity_container in layout?");
        }
    }

    public void insertFragment(Fragment fragment) {
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction()
                    .replace(container.getId(), fragment, fragment.getClass().getSimpleName());
            transaction.commitAllowingStateLoss();
        }
    }

    private void initFragment() {
        Fragment fragment = getFragment();
        if (fragment != null) {
            try {
                this.fragment = fragment;
                insertFragment(fragment);
            } catch (Exception e) {
                Log.e(BaseActivity.class.getCanonicalName(), "Can't insert fragment " + fragment.getClass().getCanonicalName(), e);
            }
        }
    }

    protected View findContainerView() {
        return findViewById(R.id.base_activity_container);
    }

    protected abstract BaseFragment getFragment();

}
