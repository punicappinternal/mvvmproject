# gradle-simple

[![](https://jitpack.io/v/org.bitbucket.punicappinternal/mvvmproject.svg)](https://jitpack.io/#org.bitbucket.punicappinternal/mvvmproject)

Punicapp MVVM Pattern implementation with android LiveCycle and ViewModel architecture components

To install the library add: 
 
```
   allprojects {
       repositories {
           maven { url 'https://jitpack.io' }
       }
   }
   dependencies {
           compile 'org.bitbucket.punicappinternal:mvvmproject:1.0.9.3'
   }
```

Under [MIT License](https://bitbucket.org/punicappinternal/mvvmproject/src/master/LICENSE.md)