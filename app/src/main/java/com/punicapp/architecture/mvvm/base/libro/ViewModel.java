package com.punicapp.architecture.mvvm.base.libro;

import android.os.Bundle;

public class ViewModel implements IViewModel {
    protected IHandleActionListener viewModelListener;
    private static IHandleActionListener defaultViewModelListener = new LiveCycleBasedActionHandler() {
        @Override
        protected boolean onAction(String actionId, Bundle bundle) {
            return true;
        }
    };

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onResume(IHandleActionListener listener) {
        this.viewModelListener = listener;
    }

    @Override
    public void onPause() {
        unregisterViewModelListener();
    }

    @Override
    public void onDestroy() {

    }

    private void unregisterViewModelListener() {
        viewModelListener = defaultViewModelListener;
    }
}
