package com.punicapp.architecture.mvvm.livecycle;

import com.punicapp.architecture.mvvm.databinding.MainAcBinding;
import com.punicapp.mvvm.android.BaseActivity;
import com.punicapp.mvvm.android.BaseFragment;

public class MainActivity extends BaseActivity<MainAcBinding> {
    @Override
    protected BaseFragment getFragment() {
        return new MainFragment();
    }
}
