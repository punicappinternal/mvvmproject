package com.punicapp.architecture.mvvm.base;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.punicapp.architecture.mvvm.R;

public class BaseActivity extends AppCompatActivity {

    private ViewGroup rootView;
    private View fragmentC;
    private Fragment fragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_ac);
        onCreateContent();
    }


    protected void onCreateContent() {
        rootView = findViewById(R.id.main_ac);
        fragmentC = findViewById(R.id.base_activity_container);
    }

    public void insertFragment(Fragment fragment) {
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction()
                    .replace(fragmentC.getId(), fragment, fragment.getClass().getSimpleName());
            transaction
                    .commitAllowingStateLoss();
        }
    }

    private void initFragment() {
        Fragment fragment = getFragment();
        if (fragment != null) {
            try {
                this.fragment = fragment;
                insertFragment(fragment);
            } catch (Exception e) {
                Log.e(BaseActivity.class.getCanonicalName(), "Can't insert fragment " + fragment.getClass().getCanonicalName(), e);
            }
        }
    }

    private Fragment getFragment() {
        return new BaseFragment();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initFragment();
    }

}
