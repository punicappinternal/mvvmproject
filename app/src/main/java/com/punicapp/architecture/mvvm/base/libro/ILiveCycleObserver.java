package com.punicapp.architecture.mvvm.base.libro;

public interface ILiveCycleObserver {
    void onStart();

    void onStop();

    void onResume(IHandleActionListener listener);

    void onPause();

    void onDestroy();
}
