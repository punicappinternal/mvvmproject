package com.punicapp.architecture.mvvm.livecycle;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.punicapp.architecture.mvvm.BR;
import com.punicapp.architecture.mvvm.R;
import com.punicapp.mvvm.actions.UIActionConsumer;
import com.punicapp.mvvm.android.BaseFragment;

import io.reactivex.functions.Consumer;

public class MainFragment extends BaseFragment<BaseViewModel> {

    private ViewDataBinding binding;
    private BaseViewModel vModel;

    @Override
    protected void fillHandlers(UIActionConsumer consumer) {
        consumer.<String>register("A", new Consumer<String>() {
            @Override
            public void accept(String data) throws Exception {
                Toast.makeText(MainFragment.this.getActivity(), "STRING=" + data, Toast.LENGTH_SHORT).show();
            }
        }).<Integer>register("B", new Consumer<Integer>() {
            @Override
            public void accept(Integer data) throws Exception {
                Toast.makeText(MainFragment.this.getActivity(), "INT=" + data, Toast.LENGTH_SHORT).show();
            }
        }).<Float>register("C", new Consumer<Float>() {
            @Override
            public void accept(Float data) throws Exception {
                Toast.makeText(MainFragment.this.getActivity(), "FLOAT=" + data, Toast.LENGTH_SHORT).show();
            }
        }).<Void>register("D", new Consumer<Void>() {
            @Override
            public void accept(Void data) throws Exception {
                Toast.makeText(MainFragment.this.getActivity(), "VOID=" + data, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected View initBinding(LayoutInflater inflater, ViewGroup container, BaseViewModel vModel) {
        this.vModel = vModel;
        binding = DataBindingUtil.inflate(inflater, R.layout.base_lifecycle_fr, container, false);
        binding.setVariable(BR.viewModel, vModel);


        return binding.getRoot();
    }
}
