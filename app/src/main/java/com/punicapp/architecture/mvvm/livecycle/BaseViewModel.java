package com.punicapp.architecture.mvvm.livecycle;

import android.app.Application;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;
import android.util.Log;

import com.punicapp.mvvm.android.AppViewModel;
import com.punicapp.mvvm.actions.UIAction;

public class BaseViewModel extends AppViewModel {

    private BaseModel model;

    private ObservableField<String> text1 = new ObservableField<>();
    private ObservableField<String> text2 = new ObservableField<>();
    private ObservableField<String> text3 = new ObservableField<>();

    private String btn = "SUBMIT";

    public BaseViewModel(@NonNull Application application) {
        super(application);
        model = new BaseModel();
    }

    public ObservableField<String> getText1() {
        return text1;
    }

    public void setText1(ObservableField<String> text1) {
        this.text1 = text1;
    }

    public ObservableField<String> getText2() {
        return text2;
    }

    public void setText2(ObservableField<String> text2) {
        this.text2 = text2;
    }

    public ObservableField<String> getText3() {
        return text3;
    }

    public void setText3(ObservableField<String> text3) {
        this.text3 = text3;
    }

    public void onTap1() {
        model.setVal1(text1.get() + "1");
        model.setVal2(text2.get());
        model.setVal3(text3.get());

        text1.set("");
        text2.set("");
        text3.set("");

        processor.onNext(new UIAction("A", "SSA"));
    }

    public void onTap2() {
        model.setVal1(text1.get());
        model.setVal2(text2.get());
        model.setVal3(text3.get());

        text1.set("");
        text2.set("");
        text3.set("");

        processor.onNext(new UIAction("B", 1));
    }

    public void onTap3() {
        model.setVal1(text1.get());
        model.setVal2(text2.get());
        model.setVal3(text3.get());


        text1.set("");
        text2.set("");
        text3.set("");

        processor.onNext(new UIAction("C", 3.14F));
    }
    public void onTap4() {
        model.setVal1(text1.get());
        model.setVal2(text2.get());
        model.setVal3(text3.get());


        text1.set("");
        text2.set("");
        text3.set("");

        processor.onNext(new UIAction("D"));
    }

    public String getBtn() {
        return btn;
    }

    public void setBtn(String btn) {
        this.btn = btn;
    }

    public BaseModel getModel() {
        return model;
    }
}
