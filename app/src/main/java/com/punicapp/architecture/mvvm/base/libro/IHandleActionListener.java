package com.punicapp.architecture.mvvm.base.libro;


import android.os.Bundle;

public interface IHandleActionListener {
    boolean onHandleAction(String actionId, Bundle bundle);
}
