package com.punicapp.architecture.mvvm.livecycle;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.punicapp.architecture.mvvm.R;
import com.punicapp.architecture.mvvm.databinding.BaseLifecycleABinding;

public class VMTestActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BaseLifecycleABinding db = DataBindingUtil.setContentView(this, R.layout.base_lifecycle_a);
        setContentView(db.getRoot());
        ViewModelProvider provider = ViewModelProviders.of(this);
        BaseViewModel baseViewModel = provider.get(BaseViewModel.class);
        db.setViewModel(baseViewModel);
    }
}
