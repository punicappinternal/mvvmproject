package com.punicapp.architecture.mvvm.livecycle;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.punicapp.architecture.mvvm.R;
import com.punicapp.mvvm.actions.UIActionConsumer;
import com.punicapp.mvvm.android.BaseFragment;
import com.punicapp.mvvm.android.AppViewModel;

public class AnotherMainFragment extends BaseFragment {

    private ViewDataBinding binding;

    @Override
    protected void fillHandlers(UIActionConsumer consumer) {
        //do nothing
    }

    @Override
    protected View initBinding(LayoutInflater inflater, ViewGroup container, AppViewModel vModel) {
        binding = DataBindingUtil.inflate(inflater, R.layout.base_lifecycle_fr, container, false);
        return binding.getRoot();
    }
}
