package com.punicapp.architecture.mvvm.base;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.punicapp.architecture.mvvm.BR;
import com.punicapp.architecture.mvvm.R;
import com.punicapp.architecture.mvvm.base.libro.IHandleActionListener;
import com.punicapp.architecture.mvvm.base.libro.ILiveCycleObserver;
import com.punicapp.architecture.mvvm.base.libro.LiveCycleBasedActionHandler;
import com.punicapp.architecture.mvvm.databinding.BaseFrBinding;

public class BaseFragment extends Fragment {
    protected View rootView;
    protected ILiveCycleObserver iViewModel;
    protected IHandleActionListener listener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listener = initListener();
    }

    protected IHandleActionListener initListener() {
        return new LiveCycleBasedActionHandler() {
            @Override
            protected boolean onAction(String actionId, Bundle bundle) {
                return true;
            }
        };

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        BaseFrBinding binding = DataBindingUtil.inflate(inflater, R.layout.base_fr, container, false);
        rootView = binding.getRoot();
        iViewModel = new BaseViewModel();
        binding.setVariable(BR.viewModel, iViewModel);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        iViewModel.onResume(listener);
    }

    @Override
    public void onPause() {
        super.onPause();
        iViewModel.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
        iViewModel.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        iViewModel.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        iViewModel.onDestroy();
    }


}
