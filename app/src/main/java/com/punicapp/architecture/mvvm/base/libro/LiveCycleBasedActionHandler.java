package com.punicapp.architecture.mvvm.base.libro;

import android.os.Bundle;

public abstract class LiveCycleBasedActionHandler implements IHandleActionListener {


    @Override
    public boolean onHandleAction(String actionId, Bundle bundle) {
        return onAction(actionId, bundle);
    }

    protected abstract boolean onAction(String actionId, Bundle bundle);
}
